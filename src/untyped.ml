(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-34-37-39-32"]

let debug = 3

(****************************************************************)
(* Simple types                                                 *)
(****************************************************************)

(* *Deep* embedding of types:

<<<
    τ, σ ::= Ω
           | τ → σ
>>>
 *)

type tp =
  | Base
  | Arr of tp * tp
[@@deriving show]

(* Define [typ1] as [Ω → Ω] *)

let typ1 () = Arr (Base, Base)

(* Define [typ2] as [(Ω → Ω) → Ω → Ω] *)

let typ2 () = Arr(Arr(Base, Base), Arr(Base, Base))

let _ =
  if debug > 0
  then (
    Format.printf "tp1 = %a\n" pp_tp (typ1 ()) ;
    Format.printf "tp2 = %a\n" pp_tp (typ2 ()) )


(****************************************************************)
(* Source language: λ-terms                                     *)
(****************************************************************)

(* *Deep* embedding of terms using weak/parametric higher-order
   abstract syntax (HOAS):

<<<
    t, u ::= x
           | λ x. t
           | t u
>>>
 *)

type 'a tm =
  | Var of 'a
  | Lam of ('a -> 'a tm)
  | App of 'a tm * 'a tm
(* Hint: ['a] ranges over the set of variables,
         piggy-back on OCaml for managing binders! *)

let rec pp_tm gensym pp_var oc = function
  | Var x -> pp_var oc x
  | Lam f ->
    let x = gensym () in
    Format.fprintf oc "(λ%a. %a)" pp_var x (pp_tm gensym pp_var) (f x)
  | App (f1, f2) -> Format.fprintf oc "%a %a" (pp_tm gensym pp_var) f1 (pp_tm gensym pp_var) f2

let (!!) f = Lam (fun x -> f (Var x))
let (!!!) f = Lam (fun x -> Lam (fun y -> f (Var x) (Var y)))
let (!!!!) f = Lam (fun x -> Lam (fun y -> Lam (fun z -> f (Var x) (Var y) (Var z))))

let (let$) f rem = rem (!! f)
let (let$$) f rem = rem (!!! f)
let (let$$$) f rem = rem (!!!! f)

let (@) f g = App (f, g)

(* Define [tm1] as [λ x. x] *)
let tm1 () = !! Fun.id

(* Define [tm2] as [λ f. λ x. f x] *)
let tm2 () = !!!(fun f x -> f @ x)

(* Define [tm3] as [λ x. (λ y. y) x] *)
let tm3 () = !!(fun x -> tm1 () @ x)

let _ =
  let gensym =
    let x = ref 0 in
    fun () ->
      incr x ;
      "x" ^ string_of_int !x
  in
  let pp_var oc s = Format.fprintf oc "%s" s in
  let pp_tm_str = pp_tm gensym pp_var in
  if debug > 1
  then (
    Format.printf "tm1 = %a\n" pp_tm_str (tm1 ()) ;
    Format.printf "tm2 = %a\n" pp_tm_str (tm2 ()) ;
    Format.printf "tm3 = %a\n" pp_tm_str (tm3 ()) )


(****************************************************************)
(* Intermediate language of values                              *)
(****************************************************************)

(* *Shallow* embedding of values in weak-head normal form:
   [https://en.wikipedia.org/wiki/Lambda_calculus_definition#Weak_head_normal_form]

<<<
    v ::= λ x. v
        | base

>>>
 *)

(* We are polymorphic in the representation ['b] of base values for now *)

exception RuntimeError

type 'b vl =
  | VFun of ('b vl -> 'b vl)
  | VBase of 'b

let (!&) f = VFun f
let (!!&) f = VFun (fun x -> VFun (f x))
let (!!!&) f = VFun (fun x -> VFun (fun y -> VFun (f x y)))

let (let&) f rem = rem (!! f)
let (let&&) f rem = rem (!!! f)
let (let&&&) f rem = rem (!!!! f)

let (@&) f x = match f with
  | VFun f -> f x
  | VBase _ -> raise RuntimeError


(* Define [vl1] as [λ x. x] *)

let vl1 () = !& Fun.id

(* Define [vl2] as [λ f. λ x. f x] *)

let vl2 () = !!& (fun f x -> f @& x)

(* Define [vl3] as [λ x. (λ y. y) x] *)
let vl3 () = !!& (fun f x -> vl1 () @& x)

(****************************************************************)
(* Target language: β-normal λ-terms                            *)
(****************************************************************)

(* *Deep* embedding of β-normal terms using weak/parametric
   higher-order abstract syntax (HOAS):
   [https://en.wikipedia.org/wiki/Beta_normal_form]

<<<
    nf ::= at
         | λ x. nf

    at ::= at nf
         | x
>>>
*)

(* Hint: ['a] ranges over the set of variables,
   piggy-back on OCaml for managing binders! *)

type 'a nf =
    At of 'a at
  | NLam of ('a -> 'a nf)

(* NYI *)
and 'a at =
  | AtApp of 'a at * 'a nf
  | Var of 'a


(* NYI *)
let rec pp_nf gensym pp_var oc = function
  | At at -> pp_at gensym pp_var oc at
  | NLam f ->
    let x = gensym () in
    Format.fprintf oc "(λ%a. %a)" pp_var x (pp_nf gensym pp_var) (f x)

and pp_at gensym pp_var oc = function
  | AtApp (at, nf) ->
    Format.fprintf oc "%a %a" (pp_at gensym pp_var) at (pp_nf gensym pp_var) nf
  | Var a -> pp_var oc a

let rec equal_nf gensym equal_var nf1 nf2 : bool =
  match nf1, nf2 with
  | At at1, At at2 -> equal_at gensym equal_var at1 at2
  | NLam f1, NLam f2 ->
    let x = gensym () in
    equal_nf gensym equal_var (f1 x) (f2 x)
  | _ -> false

and equal_at gensym equal_var at1 at2 : bool =
  match at1, at2 with
  | AtApp (at1, nf1),AtApp(at2, nf2) ->
    equal_at gensym equal_var at1 at2 && equal_nf gensym equal_var nf1 nf2
  | Var x1, Var x2 -> equal_var x1 x2
  | _ -> false

let (!|) f = NLam (fun x -> f (Var x))
let (!!|) f = NLam (fun x -> NLam (fun y -> f (Var x) (Var y)))
let (!!!|) f = NLam (fun x -> NLam (fun y -> NLam (fun z -> f (Var x) (Var y) (Var z))))

let (let|) f rem = rem (!! f)
let (let||) f rem = rem (!!! f)
let (let|||) f rem = rem (!!!! f)

let (@|) f x = AtApp (f, x)

(* Define [nf1] as [λ x. x] *)

let nf1 () = !| (fun x -> At x)

(* Define [nf2] as [λ f. λ x. f x] *)

let nf2 () = !!|(fun f x -> At (f @| At x))

(* Define [nf3] as [λ x. (λ y. y) x] *)
let nf3 () = failwith "Nope"

let _ =
  let gensym =
    let x = ref 0 in
    fun () ->
      incr x ;
      "x" ^ string_of_int !x
  in
  let pp_var oc s = Format.fprintf oc "%s" s in
  let pp_nf_str = pp_nf gensym pp_var in
  if debug > 2
  then (
    Format.printf "nf1 = %a\n" pp_nf_str (nf1 ()) ;
    Format.printf "nf2 = %a\n" pp_nf_str (nf2 ()) )


let%test _ =
  let gensym =
    let x = ref 0 in
    fun () ->
      incr x ;
      !x
  in
  let nf1 = nf1 () in
  let nf2 = nf2 () in
  not (equal_nf gensym ( = ) nf1 nf2)

(****************************************************************)
(* Evaluation function: from source to intermediate             *)
(****************************************************************)

let rec eval : type a. a vl tm -> a vl = function
  | Var v -> v
  | Lam f -> !& (fun v -> eval (f v))
  | App (f, x) -> (eval f) @& (eval x)


(****************************************************************)
(* reify and reflect: from intermediate to target               *)
(****************************************************************)

let rec reify : type a. tp -> a at vl -> a nf = fun a v -> failwith "NYI"

and reflect : type a. tp -> a at -> a at vl = fun a r -> failwith "NYI"

let%test _ =
  let gensym =
    let x = ref 0 in
    fun () ->
      incr x ;
      !x
  in
  let n1 = reify (Arr (Base, Base)) (VFun (fun x -> x)) in
  let n2 = nf1 () in
  equal_nf gensym ( = ) n1 n2

let%test _ =
  let gensym =
    let x = ref 0 in
    fun () ->
      incr x ;
      !x
  in
  let n1 =
    reify (Arr (Arr (Base, Base), Arr (Base, Base))) (VFun (fun x -> x))
  in
  let n2 = nf2 () in
  equal_nf gensym ( = ) n1 n2

(****************************************************************)
(* Normalization: from term to normal form                      *)
(****************************************************************)

let nbe : type a. tp -> a at vl tm -> a nf = fun a m -> failwith "NYI"

let gensym =
  let x = ref 0 in
  fun () ->
    incr x ;
    !x


let%test _ =
  let typ1 = typ1 () in
  let tm1 = tm1 () in
  let n1 = nbe typ1 tm1 in
  let n2 = nf1 () in
  equal_nf gensym ( = ) n1 n2

let%test _ =
  let typ2 = typ2 () in
  let tm2 = tm2 () in
  let n1 = nbe typ2 tm2 in
  let n2 = nf2 () in
  equal_nf gensym ( = ) n1 n2

let%test _ =
  let typ2 = typ2 () in
  let tm1 = tm1 () in
  let n1 = nbe typ2 tm1 in
  let n2 = nf2 () in
  equal_nf gensym ( = ) n1 n2

let%test _ =
  let typ1 = typ1 () in
  let tm3 = tm3 () in
  let n1 = nbe typ1 tm3 in
  let n2 = nf1 () in
  equal_nf gensym ( = ) n1 n2

let%test _ =
  let typ2 = typ2 () in
  let tm3 = tm3 () in
  let n1 = nbe typ2 tm3 in
  let n2 = nf2 () in
  equal_nf gensym ( = ) n1 n2

(****************************************************************)
(* Public API                                                   *)
(****************************************************************)

type vars

type x = vars at vl

type term = x tm

type normal = vars nf

let norm : tp -> term -> normal = nbe
